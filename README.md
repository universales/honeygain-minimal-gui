# Honeygain Minimal GUI
Honeygain Minimal GUI is a simple interface for Honeygain.<br>
<img src="https://i.imgur.com/YxZsB9m.png"><br>
The graphical interface eliminates the need to use commands.<br><br>
**Docker installation is required**<br><br>
Install it from the following link:<br>
https://docs.docker.com/get-docker/<br><br>

**How to run the graphical interface.**<br><br>
1 - Download [Honeygain.sh](https://gitlab.com/universales/honeygain-minimal-gui/-/raw/main/Honeygain.sh?inline=false)<br>
2 - Run it through the terminal 'sh Honeygain.sh'<br><br>
All ready :)<br><br>

**It is recommended to add execution permission to the script although it is not necessary.**

**Changelog**

**V0.1** - Initial version.
