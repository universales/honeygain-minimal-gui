#!/bin/bash

inicio(){
opcion=$(whiptail --title "Honeygain Minimal GUI | V0.1" --menu "Select an option in the menu:" 15 60 3 \
"1" "Enable and start docker service." \
"2" "Check docker service status." \
"3" "Run Honeygain in the foreground and enable auto-start."  3>&1 1>&2 2>&3)

exitstatus=$?
if [ $exitstatus = 0 ]; then
    if [ $opcion = "1" ]; then
        sudo systemctl start docker.service;
        sudo systemctl enable docker.service;
        mensajeinfo "Docker service started and enabled";
        inicio;
    fi
    if [ $opcion = "2" ]; then
        estado=$(systemctl status docker.service);
        mensajeinfo "$estado";
        inicio;
    fi
    if [ $opcion = "3" ]; then
        email=$(whiptail --title "Honeygain Minimal GUI | V0.1" --inputbox "Email: " 15 60  3>&1 1>&2 2>&3)
        status=$?
        if [ $status = 0 ]; then
            if [ $email != "" ]; then
                pass=$(whiptail --title "Honeygain Minimal GUI | V0.1" --inputbox "Password: " 15 60  3>&1 1>&2 2>&3)
                status=$?
                if [ $status = 0 ]; then
                    if [ $pass != "" ]; then
                        device_name=$(whiptail --title "Honeygain Minimal GUI | V0.1" --inputbox "Device Name: " 15 60  3>&1 1>&2 2>&3)
                        status=$?
                        if [ $status = 0 ]; then
                            if [ $device_name != "" ]; then
                                docker=$(sudo docker run -d --restart unless-stopped honeygain/honeygain -tou-accept -email "$email" -pass "$pass" -device "$device_name";);
                                mensajeinfo "Done... $docker";
                            fi
                            inicio;
                        else
                            inicio;
                        fi
                    fi
                    inicio;
                else
                    inicio;
                fi
            fi
            inicio;
        else
            inicio;
        fi
    fi


else
    exit 1;
fi
}

mensajeinfo(){
whiptail --title "Honeygain Minimal GUI | V0.1" --msgbox "$1" 30 60
}

if [ -n "$(type docker)" ]; then
    inicio;
else
    mensajeinfo "Docker is not installed on your system, please install it to continue.";
fi
exit 1;
